#!/bin/bash

set -eu

cat sources.txt | grep --invert-match '^$' | grep --invert-match '^#' > sources.txt.filtered
mv sources.txt.filtered sources.txt

if [ -n "${SOURCE+x}" ]; then
    # if FILE env variable set, then process only the source listed
    echo "Processing single source $SOURCE"
    grep --fixed-strings "$SOURCE" < sources.txt > download.txt
elif [ -n "${OVERWRITE+x}" ]; then
    # if OVERWRITE env variable set, then process all sources
    cp sources.txt download.txt

    echo "Processing all files"
    cat download.txt
else
    # otherwise only process those sources not existing
    b2 ls $B2_BUCKET | sed 's/\.tiff$//' > existing.txt
    grep -f existing.txt --fixed-strings --invert-match < sources.txt > download.txt

    echo "Skipping existing files"
    cat existing.txt
    echo "Files to be processed"
    cat download.txt
fi

mkdir -p zip
while read -r url filename; do
    if [ -z "${filename+x}" ]; then
        b=`basename $url`
    else
        b=`basename $filename`
    fi
    echo $b
    wget --quiet -O zip/$b "$url?dl=1"
done <download.txt

rm -f download.txt
