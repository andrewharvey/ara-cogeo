#!/bin/bash

b2 ls $B2_BUCKET | sed "s/^/https:\/\/f001.backblazeb2.com\/file\/$B2_BUCKET\//" > file_list.txt

# sort by date
cat file_list.txt | cut -d'_' -f3 | cut -d'.' -f1 | cut -d'-' -f1 | cut --output-delimiter="-" -c1-4,5-6,7-8 | paste -d' ' file_list.txt - | sort --field-separator=' ' -k2 --version-sort --reverse | cut -d' ' -f1 > file_list.sorted.txt

mv file_list.sorted.txt file_list.txt

#cat file_list.txt | sed "s/^/\/vsicurl\//" > file_list.vsi.txt
#gdalbuildvrt -input_file_list file_list.vsi.txt mosaic.vrt
#rm -f file_list.vsi.txt

cat file_list.txt | cogeo-mosaic create - > mosaic.json
cat file_list.txt | cogeo-mosaic footprint > footprint.geojson

jq --raw-input --slurp 'split("\n") | .[0:-1]' file_list.txt > file_list.json

curl -X POST -d @mosaic.json https://mosaic.cogeo.xyz/add
