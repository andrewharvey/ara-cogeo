#!/bin/bash

set -eu

processSingle() {
    f=$1
    b=`basename $f .zip`
    echo $b
    tmp=`mktemp -d`
    unzip -j "$f" -d "$tmp"

    # clean up zip file no longer needed
    rm -rf $f

    # fix projections, gdal doesn't read the .prj files normally
    for jpg in $tmp/*.jpg; do
        srs="EPSG:28354"
        tile=`basename $jpg .jpg`
        case $b in
        VisitorsCentre*)
            srs="EPSG:28353"
            ;;
        WesternRiver*)
            srs="EPSG:28353"
            ;;
        BreakneckRiver*)
            srs="EPSG:28353"
            ;;
        CygnetPark*)
            srs="EPSG:28353"
            ;;
        esac
        gdal_translate -of 'VRT' -a_srs "$srs" $jpg $jpg.vrt
        nearblack -of GTiff -co COMPRESS=JPEG -co TILED=YES -color 255,255,255 -color 192,192,192 -near 10 -setalpha -o $jpg.tiff $jpg.vrt
    done

    # build mosaic
    mosaic=`mktemp --suffix .tiff`
    echo "Build a GeoTIFF mosaic form source JPG files $mosaic"
    gdalwarp -t_srs 'EPSG:3857' -multi -wo NUM_THREADS=ALL_CPUS -co TILED=YES -co BLOCKXSIZE=256 -co BLOCKYSIZE=256 -co COMPRESS=JPEG -co BIGTIFF=YES "$tmp"/*.tiff "$mosaic"

    # clean up extracted JPG files no longer needed
    rm -rf "$tmp"

    # create cogeo
    cogeo=`mktemp --suffix .tiff`
    echo "Create a Cloud Optimized GeoTIFF $cogeo"
    rio cogeo create --cog-profile jpeg --bidx 1,2,3 --overview-resampling bilinear --resampling bilinear --web-optimized --add-mask --quiet $mosaic $cogeo

    # clean up mosaic file no longer needed
    rm -rf $mosaic

    # upload to Backblaze
    echo "Upload b2://$B2_BUCKET/$b.tiff"
    b2 upload_file $B2_BUCKET "$cogeo" "$b.tiff"

    # clean up cogeo file no longer needed
    rm -rf $cogeo
}

export -f processSingle
parallel processSingle ::: zip/*.zip
