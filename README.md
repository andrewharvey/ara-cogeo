# ara-cogeo

GitLab CI process to convert [Airborne Research Australia bushfire imagery](https://www.airborneresearch.org.au/fires-2020) into [Cloud Optimized GeoTIFFs](https://www.cogeo.org/).

Imagery is copyright [ARA - Airborne Research Australia](https://www.airborneresearch.org.au/) and licensed under the [Creative Commons BY-NC-SA 4.0 license](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode), with [permission to trace into OpenStreetMap](https://wiki.openstreetmap.org/wiki/File:OSM_waiver_AirborneResearchSouthAustralia.pdf).

The output of this process is hosted by cogeo.xyz and loaded into the [editor layer index](https://github.com/osmlab/editor-layer-index/blob/gh-pages/sources/oceania/au/sa/ARA.geojson) visible at [https://osmlab.github.io/editor-layer-index/](https://osmlab.github.io/editor-layer-index/).

## CI Stages
- mirror - Takes the source files hosted by ARA and mirror these on b2.
- build - Process all source files and output cogeo on b2.
- mosaic - Build mosaicJSON and other mosaic outputs.

Build variables:

- `SOURCE=single-named-source-to-process.zip` only process the named file instead of all.
- `OVERWRITE=yes` shall process all files even if they already exist, the default is to only process new files.
