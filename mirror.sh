#!/bin/bash

set -eu

cat sources.txt | grep --invert-match '^$' | grep --invert-match '^#' > sources.txt.filtered
mv sources.txt.filtered sources.txt

b2 ls $B2_MIRROR_BUCKET > existing.txt
grep -f existing.txt --fixed-strings --invert-match < sources.txt > mirror.txt

echo "Skipping existing files"
cat existing.txt
echo "Files to be mirrored"
cat mirror.txt

while read -r url filename; do
    if [ -z "${filename+x}" ]; then
        b=`basename $url`
    else
        b=`basename $filename`
    fi
    echo $b
    wget --quiet -O $b "$url?dl=1"

    echo "Upload b2://$B2_MIRROR_BUCKET/$b"
    b2 upload_file $B2_MIRROR_BUCKET "$b" "$b"

    rm -f "$b"
done <mirror.txt

# clean up
rm -f existing.txt mirror.txt
